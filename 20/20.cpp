#include "PrintHlp.h"
#include "Struct.h"
enum class Result
{
	SUCESS,
	FAIL,
	ERROR_WRITE,
	ERROR_READ
};
Result DoWork()
{
	return Result::SUCESS;
}

enum  Alphabet
{
	A,
	B=10,
	C=15
};


int main()
{
	print("A = ", A);
	print("B = ", B);
	print("C = ", C);
	print("Result_SUCESS = ", static_cast<int>(Result::SUCESS));
	print("Result_ERROR_WRITE = ", static_cast<int>(Result::ERROR_WRITE));
	auto dataFromResult = static_cast<Result>(3);
	if(dataFromResult==Result::ERROR_READ)
	{
		print("3 succeeded convert to Result::ERROR_READ");
	}
	Result myResult = DoWork();
	if (myResult == Result::SUCESS)
		print("DoWork good");
	else
		print("DoWork error");
	print("Work with Struct");
	Student a;
	auto* ptr = new Student {10,170,"SomeName"};
	ptr->GetInfo();
	return 0;
}
